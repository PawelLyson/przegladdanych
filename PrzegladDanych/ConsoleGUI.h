#pragma once
#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <array>
#include <climits>
#include <vector>

#include <Windows.h>

#include "DataOverviewExceptions.h"

using namespace std;

struct KeyInputTuple
{
	int VirtualCode;
	int AsciiCode;
};

enum ElementTypes
{
	kElementString,
	kElementNumber,
	kElementBool,
	kElementDate
};

class InputType
{
private:
	int ElementType;// = kElementString,Number,Date,Bool
	std::string BoolYes;// = "Tak";
	std::string BoolNo;// = "Nie";
	bool actualBool = false;

	bool IsNumber(char letter)
	{
		return letter >= int('0') && letter <= int('9');
	}
	bool IsLetter(char letter)
	{
		return (letter >= int('A') && letter <= int('Z')) || (letter >= int('a') && letter <= int('z'));
	}
	bool IsSpecial(char letter) // dla nazw
	{
		const std::string SpecialChars = "+=_-)(*&^%$#@!~`:'{}[]<>,.?| ";
		for (const auto& oneChar : SpecialChars)
		{
			if (oneChar == letter) return true;
		}
		return false;
	}

public:
	InputType()
	{
		this->ElementType = kElementString;
	}
	InputType(int type)
	{
		if (type == kElementBool) throw invalid_input_type_argument("For bool type use another consturctor (int type,string yes, string no)");
		this->ElementType = type;
	}
	InputType(int type, std::string yes, std::string no) //tylko dla bool
	{
		this->ElementType = type;
		this->BoolNo = no;
		this->BoolYes = yes;
	}
	bool IsCharValid(char letter)
	{
		if (this->ElementType == kElementString)
		{
			return (this->IsLetter(letter) || this->IsNumber(letter) || this->IsSpecial(letter));
		}
		else if (this->ElementType == kElementNumber || this->ElementType == kElementDate)
		{
			return (this->IsNumber(letter));
		}
		return false;
	}
	int GetElementType()
	{
		return this->ElementType;
	}
	void ChangeBool()
	{
		actualBool = !actualBool;
	}
	std::string GetBoolString()
	{
		if (actualBool) return BoolYes;
		return BoolNo;
	}
};

class ReadConsoleGuiInput
{
private:
	DWORD        ConsoleMode;
	INPUT_RECORD KeyEvent;
	/* Get the ConsoleWindowHandle input handle */
	HANDLE ConsoleReadHandle = GetStdHandle(STD_INPUT_HANDLE);
public:
	ReadConsoleGuiInput()
	{
		/* Preserve the original ConsoleWindowHandle ConsoleMode */
		//GetConsoleMode(ConsoleReadHandle, &ConsoleMode);
		/* Set to no line-buffering, no echo, no special-key-processing */
		SetConsoleMode(ConsoleReadHandle, 0);
	}
	~ReadConsoleGuiInput()
	{
		//SetConsoleMode(ConsoleReadHandle, ConsoleMode);
	}
	KeyInputTuple ReadKey()
	{
		while (true)
		{
			if (WaitForSingleObject(ConsoleReadHandle, 1) == WAIT_OBJECT_0)  /* if kbhit */
			{
				DWORD count;  /* ignored */
				/* Get the input KeyEvent */
				ReadConsoleInput(ConsoleReadHandle, &KeyEvent, 1, &count);
				/* Only respond to key release events */
				if ((KeyEvent.EventType == KEY_EVENT) && !KeyEvent.Event.KeyEvent.bKeyDown)
				{
					return { KeyEvent.Event.KeyEvent.wVirtualKeyCode, KeyEvent.Event.KeyEvent.uChar.AsciiChar };
					/*if (VirtualKeyboard)
					{
						return KeyEvent.Event.KeyEvent.wVirtualKeyCode;
					}
					else if (KeyEvent.Event.KeyEvent.uChar.AsciiChar != 0)
					{
						return KeyEvent.Event.KeyEvent.uChar.AsciiChar;
					}*/
				}
			}
		}
	}
};

class ConsoleGUIDrawer
{
protected:
	HANDLE ConsoleWriteHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	HWND ConsoleWindowHandle = GetConsoleWindow();
	RECT RectangleCords;
	void SetConsoleSize(int width, int height)
	{
		GetWindowRect(ConsoleWindowHandle, &RectangleCords); //stores the ConsoleWindowHandle's current dimensions
		MoveWindow(ConsoleWindowHandle, RectangleCords.left, RectangleCords.top, width, height, TRUE);
	}
	std::string GetStringWidthLength(std::string text, size_t maxLength)
	{
		return text.substr(0, maxLength) + (text.length() < maxLength ? GetRepeatedChar(32, maxLength - text.length()) : std::string(""));
	}

	void DrawEmptyBox(int posX, int posY, int width, int height)
	{
		for (int i = 0; i < height; i++)
		{
			SetCursorPos(posX, posY + i);
			this->ConsoleWriteColored(this->GetRepeatedChar(' ', width), 0 + 15 * 16);
		}
	}
public:
	ConsoleGUIDrawer()
	{
		SetConsoleSize(800, 500);
	}
	void ConsoleWriteColored(std::string text = "", int color = 15)
	{
		this->SetConsoleColor(color);
		cout << text;
	}
	std::string GetRepeatedChar(char oneChar, int amount)
	{
		std::string result = "";
		for (int i = 0; i < amount; i++) result += char(oneChar);
		return result;
	}
	std::string GetCenterString(std::string text, int maxLength)
	{
		int LengthLeft = maxLength - text.length();
		return this->GetRepeatedChar(' ', LengthLeft / 2) + text + this->GetRepeatedChar(' ', LengthLeft / 2);
	}
	COORD GetConsoleCursorPosition()
	{
		CONSOLE_SCREEN_BUFFER_INFO cbsi;
		if (GetConsoleScreenBufferInfo(ConsoleWriteHandle, &cbsi))
		{
			return cbsi.dwCursorPosition;
		}
		else
		{
			// The function failed. Call GetLastError() for details.
			COORD invalid = { 0, 0 };
			return invalid;
		}
	}
	void SetConsoleColor(int consoleColor)
	{
		SetConsoleTextAttribute(this->ConsoleWriteHandle, consoleColor);
	}
	void SetCursorPos(short x, short y)
	{
		SetConsoleCursorPosition(ConsoleWriteHandle, COORD{ x, y });
	}
	void RemoveCharsFromCursorPos(int amount = 1, int posX = -1, int posY = -1)
	{
		COORD CurPositionCursor = this->GetConsoleCursorPosition();
		if (posX != -1) CurPositionCursor.X = posX;
		if (posY != -1) CurPositionCursor.Y = posY;
		this->SetCursorPos(CurPositionCursor.X - amount, CurPositionCursor.Y);
		cout << this->GetRepeatedChar(' ', amount);
		this->SetCursorPos(CurPositionCursor.X - amount, CurPositionCursor.Y);
	}
	void DrawConsolePopup(std::string* text, int lines, int mode)
	{
		int PopupX = 32;
		int PopupY = 8;
		int PopupWidth = 40;
		for (int i = 0; i < (lines + 4); i++)
		{
			SetCursorPos(PopupX, PopupY + i);
			this->ConsoleWriteColored(this->GetRepeatedChar(' ', PopupWidth), 0 + 15 * 16);
		}
		for (int i = 0; i < lines; i++)
		{
			SetCursorPos(PopupX + 1, PopupY + i + 1);
			this->ConsoleWriteColored(text[i], 0 + 15 * 16);
		}
		if (mode == 0)
		{
			SetCursorPos(PopupX + PopupWidth / 2 - 7, PopupY + lines + 2);
			this->ConsoleWriteColored(this->GetCenterString("OK",16), 15);
		}
		else if (mode == 1)
		{
			SetCursorPos(PopupX + PopupWidth / 2 - 14, PopupY + lines + 2);
			this->ConsoleWriteColored(this->GetCenterString("NIE", 14), 15);
			this->ConsoleWriteColored("  ", 0 + 15 * 16);
			this->ConsoleWriteColored(this->GetCenterString("TAK",14), 15);
		}
	}
	void DrawConsoleTabBarFrame()
	{
#pragma warning( disable : 4309 )
		this->ConsoleWriteColored();
		SetCursorPos(0, 0);
		cout << char(201) << GetRepeatedChar(205, 4) << char(203) << GetRepeatedChar(205, 30) << char(203) << GetRepeatedChar(205, 30) << char(203) << GetRepeatedChar(205, 30) << char(203) << GetRepeatedChar(205, 4) << char(187);
		SetCursorPos(0, 1);
		cout << char(186) << " " << GetRepeatedChar(' ', 2) << " " << char(186) << GetRepeatedChar(' ', 30) << char(186) << GetRepeatedChar(' ', 30) << char(186) << GetRepeatedChar(' ', 30) << char(186) << " " << GetRepeatedChar(' ', 2) << " " << char(186);
		SetCursorPos(0, 2);
		cout << char(200) << GetRepeatedChar(205, 4) << char(202) << GetRepeatedChar(205, 30) << char(202) << GetRepeatedChar(205, 30) << char(202) << GetRepeatedChar(205, 30) << char(202) << GetRepeatedChar(205, 4) << char(188);
#pragma warning( default : 4309 )
	}

	void DrawConsoleTabBar(std::string leftNumber, std::string threeTexts[], std::string rightNumber)
	{
		this->ConsoleWriteColored();
		this->DrawConsoleTabBarFrame();
		this->ConsoleWriteColored();
		this->SetCursorPos(1, 1);
		this->ConsoleWriteColored(this->GetStringWidthLength(leftNumber, 2));
		this->SetCursorPos(6, 1);
		this->ConsoleWriteColored(this->GetStringWidthLength(threeTexts[0], 30));
		this->SetCursorPos(37, 1);
		this->ConsoleWriteColored(this->GetStringWidthLength(threeTexts[1], 30));
		this->SetCursorPos(68, 1);
		this->ConsoleWriteColored(this->GetStringWidthLength(threeTexts[2], 30));
		this->SetCursorPos(100, 1);
		this->ConsoleWriteColored(this->GetStringWidthLength(rightNumber, 2));

	}
	void DrawConsoleLegendBar(std::vector<std::string>& elementsVector)
	{
		this->SetCursorPos(0, 26);
		this->ConsoleWriteColored(elementsVector.at(0) + this->GetRepeatedChar(' ', 105 - elementsVector.at(0).length()), 15);
		this->SetCursorPos(0, 27);
		this->ConsoleWriteColored(this->GetRepeatedChar(' ', 105), 15);
		this->SetCursorPos(0, 27);
		bool First = true;
		for (std::string& element: elementsVector)
		{
			if (First) { First = false; continue; }
			this->ConsoleWriteColored(element, 0 + 15 * 16);
			this->ConsoleWriteColored(" ");
		}
	}
	void DrawFrameMiddle()
	{
#pragma warning( disable : 4309 )
		int i = 2;
		this->SetCursorPos(0, ++i);
		cout << char(201) << GetRepeatedChar(205, 102) << char(187);
		while (i < 5)
		{
			this->SetCursorPos(0, ++i);
			cout << char(186) << GetRepeatedChar(' ', 102) << char(186);
		}
		this->SetCursorPos(0, ++i);
		cout << char(204) << GetRepeatedChar(205, 102) << char(185);
		while (i < 21)
		{
			this->SetCursorPos(0, ++i);
			cout << char(186) << GetRepeatedChar(' ', 102) << char(186);
		}
		this->SetCursorPos(0, ++i);
		cout << char(204) << GetRepeatedChar(205, 102) << char(185);
		while (i < 24)
		{
			this->SetCursorPos(0, ++i);
			cout << char(186) << GetRepeatedChar(' ', 102) << char(186);
		}
		this->SetCursorPos(0, ++i);
		cout << char(200) << GetRepeatedChar(205, 102) << char(188);
#pragma warning( default : 4309 )
	}
	void DrawSmallBookInfo(int booksAmount, std::string bookName, std::string authorName, bool isDown = false)
	{
		int ScreenLine;
		if (isDown)ScreenLine = 23; // dolna informacja
		else ScreenLine = 4; // g�rna informacja

		this->SetCursorPos(1, ScreenLine);
		this->ConsoleWriteColored("#" + std::to_string(booksAmount) + " ", 15);
		this->ConsoleWriteColored(bookName, 15);
		this->SetCursorPos(1, ScreenLine + 1);
		this->ConsoleWriteColored(authorName, 15);
	}
	void DrawBigBookInfo(std::vector<std::string> booksElements, size_t actualBookNumber, bool isChecked)
	{
		//TODO: Tekst powinnien pochodzi� spoza metody oraz powinno to si� odbywa� w p�tli
		this->SetCursorPos(1, 7);
		this->ConsoleWriteColored("#", 15);
		this->ConsoleWriteColored(std::to_string(actualBookNumber), 15);
		this->SetCursorPos(1, 8);
		this->ConsoleWriteColored("Zaznaczone: ", 15);
		this->ConsoleWriteColored((isChecked ? "Tak":"Nie"), 15);
		this->SetCursorPos(1, 11);
		this->ConsoleWriteColored("Tytul: ", 15);
		this->ConsoleWriteColored(booksElements[0], 15);
		this->SetCursorPos(1, 12);
		this->ConsoleWriteColored("Autor: ", 15);
		this->ConsoleWriteColored(booksElements[1], 15);
		this->SetCursorPos(1, 13);
		this->ConsoleWriteColored("Gatunek: ", 15);
		this->ConsoleWriteColored(booksElements[2], 15);
		this->SetCursorPos(1, 14);
		this->ConsoleWriteColored("Wydawnictwo: ", 15);
		this->ConsoleWriteColored(booksElements[3], 15);
		this->SetCursorPos(1, 15);
		this->ConsoleWriteColored("Data premiery: ", 15);
		this->ConsoleWriteColored(booksElements[4], 15);
		this->SetCursorPos(1, 16);
		this->ConsoleWriteColored("Rok Wydania: ", 15);
		this->ConsoleWriteColored(booksElements[5], 15);
		this->SetCursorPos(1, 17);
		this->ConsoleWriteColored("Ilosc strony: ", 15);
		this->ConsoleWriteColored(booksElements[6], 15);
		this->SetCursorPos(1, 18);
		this->ConsoleWriteColored("Oprawa: ", 15);
		this->ConsoleWriteColored(booksElements[7], 15);
		this->SetCursorPos(1, 19);
		this->ConsoleWriteColored("Jezyk wydania: ", 15);
		this->ConsoleWriteColored(booksElements[8], 15);
	}
	void DrawInputPopup(std::string text)
	{
		int PopupX = 32;
		int PopupY = 8;
		int PopupWidth = 40;
		int PopupHeight = 6;

		this->DrawEmptyBox(PopupX, PopupY, PopupWidth, PopupHeight);

		SetCursorPos(PopupX + 1, PopupY + 2);
		this->ConsoleWriteColored(this->GetRepeatedChar(' ', 38), 15);

		SetCursorPos(PopupX + 1, PopupY + 1);
		this->ConsoleWriteColored(text, 0 + 15 * 16);

		SetCursorPos(PopupX + PopupWidth / 2 - 7, PopupY + 4);
		this->ConsoleWriteColored("      Ok      ", 15);
	}
	void DrawEditPopupFrame(int posX, int posY, int width, int height)
	{
		short ColorConsole = 0 + 16 * 15;
		SetCursorPos(posX, posY);
#pragma warning( disable : 4309 )
		int i = posY;
		this->ConsoleWriteColored("", ColorConsole);
		cout << char(201) << GetRepeatedChar(205, width - 2 ) << char(187);
		while (i < height)
		{
			this->SetCursorPos(posX, ++i);
			cout << char(186) << GetRepeatedChar(' ', width - 2 ) << char(186);
		}
		this->SetCursorPos(posX, ++i);
		cout << char(200) << GetRepeatedChar(205, width - 2 ) << char(188);

#pragma warning( default : 4309 )
	}
	void DrawEditPopupData(int posX, int posY, short color, std::vector<std::string>& elementsNames)
	{
		int PopupX = posX + 1;
		int PopupY = posY + 1;
		this->SetCursorPos(PopupX, PopupY);
		this->ConsoleWriteColored(this->GetRepeatedChar(' ',40) + "Edytor", color); //TODO: wyci�gn�� z tej metody Edytor
		PopupX += 1;
		int linesPos = PopupY + 1;
		for (const std::string& element : elementsNames)
		{
			this->SetCursorPos(posX, linesPos++);
			linesPos++;
			this->ConsoleWriteColored(element, color);
		}
	}
	void DrawRadioButton(std::string& title, std::vector<std::string>& textToPrint)
	{
		int PopupX = 32;
		int PopupY = 8;
		int PopupWidth = 40;
		int PopupHeight = 12;

		this->DrawEmptyBox(PopupX, PopupY, PopupWidth, PopupHeight);

		PopupX += 1;
		PopupY += 1;
		this->SetCursorPos(PopupX, PopupY);
		this->ConsoleWriteColored(this->GetCenterString(title, PopupWidth - 2), 0 + 15 * 16);

		PopupX += 1;
		PopupY += 1;
		for (const auto& text : textToPrint)
		{
			this->SetCursorPos(PopupX, PopupY++);
			this->ConsoleWriteColored("   |" + text, 0 + 15 * 16);
		}

		PopupY += 1;

	}

	~ConsoleGUIDrawer() {}
};

class ConsoleGUI
{
private:
	ReadConsoleGuiInput InputReader;
	ConsoleGUIDrawer ConsoleDrawer;

	std::string PrepareIfEmptyName(std::string& name)
	{
		if (name == "" || name == "*") return "{bez nazwy}";
		return name;
	}
	int YesNoKeyCatch()
	{
		while (true)
		{
			int Key = this->InputReader.ReadKey().VirtualCode;
			if (Key == int('T') || Key == VK_RETURN)
			{
				return true;
			}
			else if (Key == int('N') || Key == VK_ESCAPE)
			{
				return false;
			}
		}
	}
	void AddCharToStringAndPrint(std::string& textToChange, char letter)
	{
		textToChange += letter;
		cout << letter;
	}
	int RadioInputSelect(int elementsAmounts)
	{
		int PopupX = 32 + 2;
		int PopupY = 8 + 2;

		int elementChosen = 0;

		this->ConsoleDrawer.SetCursorPos(PopupX, PopupY);
		this->ConsoleDrawer.ConsoleWriteColored("-->", 0 + 15 * 16);

		while (true)
		{

			int Key = this->InputReader.ReadKey().VirtualCode;
			if (Key == int('T') || Key == VK_RETURN)
			{
				return elementChosen;
			}
			else if (Key == VK_UP)
			{
				if (elementChosen > 0)
				{
					elementChosen--;
					this->ConsoleDrawer.RemoveCharsFromCursorPos(3);
					this->ConsoleDrawer.SetCursorPos(PopupX, --PopupY);
					this->ConsoleDrawer.ConsoleWriteColored("-->", 0 + 15 * 16);

				}
			}
			else if (Key == VK_DOWN)
			{

				if (elementChosen < elementsAmounts - 1)
				{
					elementChosen++;
					this->ConsoleDrawer.RemoveCharsFromCursorPos(3);
					this->ConsoleDrawer.SetCursorPos(PopupX, ++PopupY);
					this->ConsoleDrawer.ConsoleWriteColored("-->", 0 + 15 * 16);
				}
			}
		}
		return 0;
	}
	std::vector<std::string> GetMultilineTextGui(int posX, int posY, size_t maxLength, int consoleColor = 15, int linesAmount = 1, int linesSkip = 0, vector<std::string> defaultValues = {}, std::vector<InputType> elementsTypes = {}) //TODO: Przemy�le� funkcje, za du�� i za du�o zmiennych
	{
		ConsoleDrawer.SetConsoleColor(consoleColor);
		std::vector<std::string> EnteredTextVector;
		EnteredTextVector.resize(linesAmount);
		if (defaultValues.size() != 0) EnteredTextVector = defaultValues;

		if (elementsTypes.size() == 0)
			for (int i = 0; i < linesAmount; i++)
				elementsTypes.push_back(kElementString);

		int ActualLineG = 0;
		for (int i = 0; i < linesAmount; i++)
		{
			this->ConsoleDrawer.SetCursorPos(posX, posY + ActualLineG);
			this->ConsoleDrawer.ConsoleWriteColored(EnteredTextVector.at(i));
			this->ConsoleDrawer.ConsoleWriteColored(this->ConsoleDrawer.GetRepeatedChar(' ', maxLength - EnteredTextVector.at(i).length()));
			ActualLineG += 1 + linesSkip;
		}
		KeyInputTuple KeyTouple;
		int ActualLine = 0;
		while (true)
		{
			ConsoleDrawer.SetCursorPos(posX + short(EnteredTextVector.at(ActualLine).length()), posY + short(ActualLine) * (1 + linesSkip));
			KeyTouple = this->InputReader.ReadKey();

			if (KeyTouple.VirtualCode == VK_UP && ActualLine > 0)
			{
				ActualLine--;
			}
			else if (KeyTouple.VirtualCode == VK_DOWN && ActualLine < (linesAmount - 1))
			{
				ActualLine++;
			}
			else if (KeyTouple.VirtualCode == VK_RETURN)
			{
				break;
			}
			else if (elementsTypes.at(ActualLine).GetElementType() == kElementString || elementsTypes.at(ActualLine).GetElementType() == kElementNumber)
			{
				if (elementsTypes.at(ActualLine).IsCharValid(KeyTouple.AsciiCode) && EnteredTextVector.at(ActualLine).size() < maxLength)
				{
					this->AddCharToStringAndPrint(EnteredTextVector.at(ActualLine), KeyTouple.AsciiCode);
				}
				else if (KeyTouple.VirtualCode == VK_BACK && EnteredTextVector.at(ActualLine).size() > 0)
				{
					EnteredTextVector.at(ActualLine).pop_back();
					this->ConsoleDrawer.RemoveCharsFromCursorPos(1);
				}
			}
			else if (elementsTypes.at(ActualLine).GetElementType() == kElementBool)
			{
				if (KeyTouple.VirtualCode == VK_LEFT || KeyTouple.VirtualCode == VK_RIGHT)
				{
					ConsoleDrawer.SetCursorPos(posX, posY + ActualLine * (1 + linesSkip));
					elementsTypes.at(ActualLine).ChangeBool();
					cout << elementsTypes.at(ActualLine).GetBoolString();  //TODO: B�edne miejsce na wprowadzenie takiej sta�ej
					EnteredTextVector.at(ActualLine) = elementsTypes.at(ActualLine).GetBoolString();
				}
			}
			else if (elementsTypes.at(ActualLine).GetElementType() == kElementDate)
			{
				if (elementsTypes.at(ActualLine).IsCharValid(KeyTouple.AsciiCode) && EnteredTextVector.at(ActualLine).size() < 10)
				{
					if (EnteredTextVector.at(ActualLine).length() == 4 || EnteredTextVector.at(ActualLine).length() == 7)
					{
						this->AddCharToStringAndPrint(EnteredTextVector.at(ActualLine), char('-'));
					}
					this->AddCharToStringAndPrint(EnteredTextVector.at(ActualLine), KeyTouple.AsciiCode);
				}
				else if (KeyTouple.VirtualCode == VK_BACK && EnteredTextVector.at(ActualLine).size() > 0)
				{
					if (EnteredTextVector.at(ActualLine).length() == 5 || EnteredTextVector.at(ActualLine).length() == 8)
					{
						EnteredTextVector.at(ActualLine).pop_back();
						this->ConsoleDrawer.RemoveCharsFromCursorPos(1);
					}
					EnteredTextVector.at(ActualLine).pop_back();
					this->ConsoleDrawer.RemoveCharsFromCursorPos(1);
				}
			}
		}
		return EnteredTextVector;
	}

public:
	ConsoleGUI() {};
	void RedrawMiddleScreen()
	{
		this->ConsoleDrawer.DrawFrameMiddle();
	}
	void RedrawTabBar(size_t actualTabNumber, std::vector<std::string>* namesList)
	{
		std::string texts[] = { "","","" };
		size_t left = 0;
		size_t right = 0;
		size_t ActualTabNumber = actualTabNumber;
		std::vector<std::string>* NamesList = namesList;
		size_t AmountSize = NamesList->size();
		if (ActualTabNumber == 1)
		{
			texts[1] = this->PrepareIfEmptyName(NamesList->at(ActualTabNumber - 1));
		}
		if (ActualTabNumber > 1)
		{
			texts[0] = this->PrepareIfEmptyName(NamesList->at(ActualTabNumber - 2)); // - 1 - 1
			texts[1] = this->PrepareIfEmptyName(NamesList->at(ActualTabNumber - 1));
			left = ActualTabNumber - 2;
		}
		if (ActualTabNumber < NamesList->size())
		{
			texts[2] = this->PrepareIfEmptyName(NamesList->at(ActualTabNumber)); // - 1 + 1
			right = NamesList->size() - ActualTabNumber - 1;
		}
		this->ConsoleDrawer.DrawConsoleTabBar(std::to_string(left), texts, std::to_string(right));
	}
	void RedrawTabBar()
	{
		this->ConsoleDrawer.DrawConsoleTabBarFrame();
	}
	void RedrawLegendBar(std::vector<std::string>& mainMenuLegendElements)
	{
		this->ConsoleDrawer.DrawConsoleLegendBar(mainMenuLegendElements);
	}
	void RedrawMiddleScreen(std::vector<std::string> mainBook, size_t actualBookNumber, bool isChecked)
	{
		this->RedrawMiddleScreen();
		this->ConsoleDrawer.DrawBigBookInfo(mainBook, actualBookNumber, isChecked);
	}
	void RedrawUpSmallInfo(size_t firstNumber, std::string firstName, std::string firstAuthor, bool down = false)
	{
		this->ConsoleDrawer.DrawSmallBookInfo(firstNumber, firstName, firstAuthor, down);
	}
	std::string PopupInputBox(std::string text) //TODDO: przerobi� na vector
	{
		ConsoleDrawer.DrawInputPopup(text);
		return this->GetMultilineTextGui(33, 10, 30).at(0);
	}
	KeyInputTuple ReadKey()
	{
		return this->InputReader.ReadKey();
	}
	int YesNoPopup(std::vector<std::string>& msg)
	{
		this->ConsoleDrawer.DrawConsolePopup(&msg.at(0), msg.size(), 1);
		return this->YesNoKeyCatch();
	}
	int YesNoPopup(std::string& msg)
	{
		this->ConsoleDrawer.DrawConsolePopup(&msg, 1, 1);
		return this->YesNoKeyCatch();
	}
	void MsgBoxShow(std::string text)
	{
		std::string textArray[1] = { text };
		this->ConsoleDrawer.DrawConsolePopup(textArray, 1, 0);
		this->YesNoKeyCatch();
	}
	void MsgBoxShow(std::vector<std::string>& msg)
	{
		this->ConsoleDrawer.DrawConsolePopup(&msg.at(0), msg.size(), 0);
		this->YesNoKeyCatch();
	}
	std::vector<std::string> DrawEditPopup(std::vector<std::string> defaultValues = {})
	{		
		std::vector<std::string> ElementsNames = { "Tytul: ","Autor: ","Gatunek: ","Wydawnictwo: ","Data premiery: ","Rok Wydania: ","Ilosc strony: ","Oprawa: ","Jezyk wydania: " }; //TODO: Przenie�c poza klase do rysowania gui np do MainMenu
		std::vector<InputType> ElementsTypes = {InputType(), InputType() , InputType() , InputType(), InputType(kElementDate), InputType(kElementNumber), InputType(kElementNumber), InputType(kElementBool,"twarda","miekka"), InputType()};
		this->ConsoleDrawer.DrawEditPopupFrame(10,4,85,22);
		this->ConsoleDrawer.DrawEditPopupData(11, 4, 0 + 16 * 15, ElementsNames); // white background black text
		return this->GetMultilineTextGui(30, 6, 60, 15, 9, 1, defaultValues, ElementsTypes);
		//this->YesNoKeyCatch();
	}
	int RadioButton(std::string title, std::vector<std::string> radioButtonText)
	{
		this->ConsoleDrawer.DrawRadioButton(title, radioButtonText);
		return this->RadioInputSelect(radioButtonText.size());
	}
};
