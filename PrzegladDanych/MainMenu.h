#pragma once
#include <iostream>
#include <string>
#include <vector>

#include "TabManager.h"
#include "ConsoleGUI.h"
#include "DataOverviewExceptions.h"

enum DrawningType
{
	kTabsElements,
	kClipboardElements,
	kBinElements,
	kSearchElements
};
class MainMenu
{
private:
	std::vector<std::string> MainMenuTabLegend = { "Narzedzia", "F1 Nowy", "F2 Otworz", "F3 Zapisz", "F4 Zmien nazwe", "F5 Zamknij plik", "F6 Otworz kosz", "F7 Otworz schowek", "F12 Wyjdz" };
	std::vector<std::string> MainMenuSingleLegend = { "Pojedyncze", "1 Nowa", "2 Edytuj", "3 Usun", "4 Wyszukaj", "5 Zaznacz", "6 Posortuj" , "7 Wytnij", "8 Kopiuj", "9 Wklej"};
	std::vector<std::string> MainMenuGroupLegend = { "Grupowe", "Q Zaznacz wszystko", "W Odznacz wszystko", "E Wytnij zaznaczone", "R Kopiuj zaznaczone", "Y Wklej" , "U Usun zaznaczone" };
	std::vector<std::vector<std::string>> MainMenuLegend; //uzupe�niane w konstruktorze

	std::vector<std::string> ClipboardMenuLegend = { "Schowek operacje:", "1 Wyczysc", "0 Wyjdz z schowka" };
	std::vector<std::string> BinMenuLegend = { "Kosz operacje:", "1 Wyczysc","2 Przywroc", "3 Wytnij", "4 Kopiuj","5 Usun", "0 Wyjdz z kosza" };

	std::vector<std::string> SearchMenuSingleLegend = { "Wyszukiwanie:", "1 Resetuj","2 Wyszukaj", "3 Wytnij", "4 Kopiuj","5 Zaznacz","6 Usun","0 Wyjdz z wyszukiwania" };
	std::vector<std::string> SearchMenuGroupLegend = { "Wyszukiwanie akcje grupowe:", "Q Zaznacz wszystko","W Odznacz wszystko", "E Wytnij zaznaczone", "R Kopiuj zaznaczone", "Y Usun zaznaczone" };
	std::vector<std::vector<std::string>> SearchMenuLegend; //uzupe�niane w konstruktorze

	std::vector<std::string> EmptyFileNameMsg = { "Nazwa pliku nie moze byc pusta!", "Zmien nazwe pliku." };
	std::vector<std::string> RestoringErrorMsg = { "Brak otwartego pliku zrodlowego.","Przywracanie niemozliwe do wykonania!" };
	std::vector<std::string> UnSavedChangesMsg = { "Zmiany nie zostaly zapisane!", "Czy na pewno chcesz zamknac plik?" };

	std::string QuitMsg = "Czy na pewno chcesz wyjsc z programu?";
	std::string SaveSuccesfulMsg = "Zapisano!";
	std::string DeleteConfirmMsg = "Czy na pewno chcesz usunac ksiazke/ki?";// , "Tego nie mozna wycofac.";
	std::string SortingAscMsg = "Czy posortowac pliki rosnaco/alfaetycznie?";
	std::string RestoringMsg = "Ksiazke przywrocono.";
	std::string ErrorDataMsg = "Blednie wpisane dane!";
	std::string ErrorDataRangeMsg = "Bledny zakres daty";
	std::string ErrorDataInsertMsg = "Niepelnie wpisana data";

	std::string ErrorOpenFileMsg = "Blad otwierania pliku!";
	std::string ErrorCloseFileMsg = "Blad zapisywania pliku!";
	std::string ErrorNoOpenFileMsg = "Brak otwartego pliku!";
	std::string ErrorNoBook = "Brak ksiazek w danym pliku!";
	std::string NewFileNameMsg = "Wprowadz nowa nazwe pliku:";
	std::string FileNameToOpenMsg = "Wprowadz nazwe pliku do otwarcia:";
	std::string RetryMsg = "Czy chcesz ponowic probe?";
	std::string WrongDataMsg = "Jedno z pol posiada bledne dane.";

	std::string TitleSorting = "Sortowanie";
	std::string TitleSearch = "Wyszukiwanie";
	std::vector<std::string> RadioButtonText = { "Tytul","Autorzy","Gatunek","Wydawnictwo","Data premiery","Rok wydania","Ilosc stron","Oprawa","Jezyk wydania" };
	std::vector<std::string> SearchInformationText = { "Wprowadz tytyul do wyszukania: ", "Wprowadz autora do wyszukania: ", "Wprowadz gatunek do wyszukania: ", "Wprowadz wydawnictwo do wyszukania: ", "Wprowadz date wydania do wyszukania: ", "Wprowadz rok wydania do wyszukania: ", "Wprowadz ilosc stron do wyszukania: ", "Czy okkladka ma byc twarda? ", "Wprowadz jezyk wydania:" };

	std::vector<std::string> OldBookData;
	std::vector<std::string> NewBookData;

	TabManager MainTabManager;
	ConsoleGUI MainGUI;

	OneTabSearch ActualTab;
	size_t ActualBookNumber;

	bool IsProgramRunning = true;
	bool IsExtraMenu = true;
	int ActualShowingLegend = 0;

	void RedrawInterface(int drawingType = kTabsElements)
	{
		if (drawingType == kTabsElements)
		{
			this->MainGUI.RedrawLegendBar(this->MainMenuLegend.at(ActualShowingLegend));

			try //Pasek zak�adek dla g��wnego menu
			{
				ActualTab = this->MainTabManager.GetTab();
				this->MainGUI.RedrawTabBar(this->MainTabManager.GetActualTabNumber(), this->MainTabManager.GetTabNamesVector());
			}
			catch (const out_of_range_tab&)
			{
				this->MainGUI.RedrawTabBar();
			}
		}
		else if (drawingType == kClipboardElements)
		{
			this->MainGUI.RedrawLegendBar(this->ClipboardMenuLegend);
			ActualTab = this->MainTabManager.GetClipboard();
			this->MainGUI.RedrawTabBar();
		}
		else if (drawingType == kBinElements)
		{
			this->MainGUI.RedrawLegendBar(this->BinMenuLegend);
			ActualTab = this->MainTabManager.GetBin();
			this->MainGUI.RedrawTabBar();
		}
		else if (drawingType == kSearchElements)
		{
			this->MainGUI.RedrawLegendBar(this->SearchMenuLegend.at(ActualShowingLegend));
			ActualTab = this->MainTabManager.GetTab();
			this->MainGUI.RedrawTabBar();
		}

		ActualBookNumber = ActualTab.GetActualBookNumber();
		//TODO: numeracja ksi�zek jest b�edna dla wyszukiwarki

		try // �rodek ekranu
		{
			if (drawingType != kSearchElements)
			{
				this->MainGUI.RedrawMiddleScreen(ActualTab.GetBookByOffset()->GetBookAsStringVector(), ActualBookNumber, ActualTab.GetBookByOffset()->IsSelected());
			}
			else
			{
				this->MainGUI.RedrawMiddleScreen(ActualTab.GetFoundBookByOffset()->GetBookAsStringVector(), ActualBookNumber, ActualTab.GetFoundBookByOffset()->IsSelected());
			}
		}
		catch (const out_of_range_book&)
		{
			this->MainGUI.RedrawMiddleScreen();
		}

		try // G�rna infomracja o poprzedniej ksi��ce
		{
			if (drawingType != kSearchElements)
			{
				this->MainGUI.RedrawUpSmallInfo(ActualBookNumber - 1, ActualTab.GetBookByOffset(-1)->GetTitle(), ActualTab.GetBookByOffset(-1)->GetAutors(), false);
			}
			else
			{
				this->MainGUI.RedrawUpSmallInfo(ActualBookNumber - 1, ActualTab.GetFoundBookByOffset(-1)->GetTitle(), ActualTab.GetFoundBookByOffset(-1)->GetAutors(), false);
			}
		}
		catch (const out_of_range_book&)//std::out_of_range& e)
		{
		}

		try// Dolna infomracja o nast�pnej ksi��ce
		{

			if (drawingType != kSearchElements)
			{
				this->MainGUI.RedrawUpSmallInfo(ActualBookNumber + 1, ActualTab.GetBookByOffset(1)->GetTitle(), ActualTab.GetBookByOffset(1)->GetAutors(), true);
			}
			else
			{
				this->MainGUI.RedrawUpSmallInfo(ActualBookNumber + 1, ActualTab.GetFoundBookByOffset(1)->GetTitle(), ActualTab.GetFoundBookByOffset(1)->GetAutors(), true);
			}
		}
		catch (const out_of_range_book&)
		{
		}
	}
	void MenuNewFile()
	{
		this->MainTabManager.AddNewTab();
	}
	void MenuOpenFile()
	{
		this->MainTabManager.AddNewTab();
		this->MainTabManager.GetTab().SetFileName(this->MainGUI.PopupInputBox(FileNameToOpenMsg));
		try
		{
			this->MainTabManager.GetTab().LoadData();
		}
		catch (const std::runtime_error&)
		{
			this->MainTabManager.RemoveTab();
			this->MainGUI.MsgBoxShow(this->ErrorOpenFileMsg);
		}
		catch (const invalid_filename&)
		{
			this->MainTabManager.RemoveTab();
			this->MainGUI.MsgBoxShow(this->EmptyFileNameMsg.at(0));
		}
	}
	void MenuSaveFile()
	{
		std::string ActualFileName = "";
		try
		{
			ActualFileName = this->MainTabManager.GetTab().GetFileName();
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
			return;
		}
		if (ActualFileName != "")
		{
			try
			{
				this->MainTabManager.GetTab().SaveData();
				this->MainGUI.MsgBoxShow(this->SaveSuccesfulMsg);
			}
			catch (const std::runtime_error&)
			{
				this->MainGUI.MsgBoxShow(this->ErrorCloseFileMsg);
			}
		}
		else
		{
			this->MainGUI.MsgBoxShow(this->EmptyFileNameMsg);
		}
	}
	void MenuChangeFileName()
	{
		try
		{
			OneTabSearch* ActualTab = &this->MainTabManager.GetTab();
			ActualTab->SetFileName(this->MainGUI.PopupInputBox(this->NewFileNameMsg));
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
	}
	void MenuCloseFile()
	{
		try
		{
			OneTabSearch* ActualTab = &this->MainTabManager.GetTab();
			if (ActualTab->IsTabChanged() && this->MainGUI.YesNoPopup(this->UnSavedChangesMsg))
			{
				this->MainTabManager.RemoveTab();
			}
			else if (!this->MainTabManager.GetTab().IsTabChanged())
			{
				this->MainTabManager.RemoveTab();
			}
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
	}
	void MenuCloseProgram()
	{
		if (this->MainGUI.YesNoPopup(this->QuitMsg))
		{
			this->IsProgramRunning = false;
		}
	}
	void MenuChangeLegendBar()
	{
		this->ActualShowingLegend++;
		if (this->ActualShowingLegend > 2) this->ActualShowingLegend = 0;
	}
	void MenuChangeActualTab(int offset)
	{
		try
		{
			this->MainTabManager.ChangeActualTabNumer(offset);
		}
		catch (const empty_list&)
		{

		}
	}
	void ChangeActualBook(int offset, OneTabSearch& oneTab, bool searching = false)
	{
		try
		{
			if (searching) oneTab.ChangeActualBookSearch(offset);
			else oneTab.ChangeActualBook(offset);
		}
		catch (const empty_list&)
		{

		}
		catch (const out_of_range_book&)
		{

		}
	}
	void MenuAddBook()
	{
		this->OldBookData.clear();
		while (true)
		{
			try
			{
				OneTabSearch* ActualTab = &this->MainTabManager.GetTab();
				this->NewBookData = this->MainGUI.DrawEditPopup(OldBookData);
				ActualTab->AddBookFromStringArray(NewBookData.data());
				return;
			}
			catch (const date_invalid_format&)
			{
				this->MainGUI.MsgBoxShow(this->ErrorDataInsertMsg);
				this->OldBookData = this->NewBookData;
			}
			catch (const date_invalid_range&)
			{
				this->MainGUI.MsgBoxShow(this->ErrorDataRangeMsg);
				this->OldBookData = this->NewBookData;
			}
			catch (const out_of_range_tab&)
			{
				this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
				return;
			}
			catch (const invalid_tab_argument&)
			{
				this->MainGUI.MsgBoxShow(this->ErrorDataMsg);
				this->OldBookData = this->NewBookData;
			}
			catch (const no_valid_data&)
			{
				this->MainGUI.MsgBoxShow(this->WrongDataMsg);
				this->OldBookData = this->NewBookData;
			}
			this->RedrawInterface();
			if (!this->MainGUI.YesNoPopup(this->RetryMsg)) return;
		}
	}
	void MenuEditBook()
	{
		try
		{
			this->OldBookData = this->MainTabManager.GetTab().GetBook()->GetBookAsStringVector();
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
			return;
		}
		while (true)
		{
			this->NewBookData = this->MainGUI.DrawEditPopup(OldBookData);
			try
			{
				this->MainTabManager.GetTab().AddBookFromStringArray(NewBookData.data(), false);
				this->MainTabManager.GetTab().RemoveOneBook();
				break;
			}
			catch (const date_invalid_format&)
			{
				this->MainGUI.MsgBoxShow(this->ErrorDataInsertMsg);
				this->OldBookData = this->NewBookData;
			}
			catch (const date_invalid_range&)
			{
				this->MainGUI.MsgBoxShow(this->ErrorDataRangeMsg);
				this->OldBookData = this->NewBookData;
			}
			catch (const out_of_range_tab&)
			{
				this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
				return;
			}
			catch (const invalid_tab_argument&)
			{
				this->MainGUI.MsgBoxShow(this->ErrorDataMsg);
				this->OldBookData = this->NewBookData;
			}
			catch (const no_valid_data&)
			{
				this->MainGUI.MsgBoxShow(this->WrongDataMsg);
				this->OldBookData = this->NewBookData;
			}
			this->RedrawInterface();
			if (!this->MainGUI.YesNoPopup(this->RetryMsg)) return;
		}
	}
	void MenuRemoveActualBook()
	{
		try
		{
			this->MainTabManager.GetTab().GetBook(); //Sprawdzanie czy zak�adka i ksi��ka istnieje
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
			return;
		}
		catch (const out_of_range_book&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoBook);
			return;
		}

		if (this->MainGUI.YesNoPopup(this->DeleteConfirmMsg))
		{
			this->MainTabManager.CutSingleBook(0, true);
		}
	}
	void MenuRemoveManyBooks()
	{
		try
		{
			this->MainTabManager.GetTab(); //Sprawdzanie czy zak�adka
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
			return;
		}

		if (this->MainGUI.YesNoPopup(this->DeleteConfirmMsg))
		{
			this->MainTabManager.CutManyBooks(true);
		}
	}
	void MenuToggleSelectBook()
	{
		try
		{
			MainTabManager.GetTab().ToggleSelectBook();
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
		catch (const out_of_range_book&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoBook);
		}
	}
	void MenuSortingTab()
	{
		try
		{
		//int SortingType = this->MainGUI.RadioButton(TitleSorting, RadioButtonText);
		//bool IsAsc = this->MainGUI.YesNoPopup(SortingAscMsg) ? false: true;
		OneTabSearch* ActualTab = &this->MainTabManager.GetTab();
		ActualTab->SortTab(this->MainGUI.RadioButton(this->TitleSorting, this->RadioButtonText));
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
	}
	void MenuCutSingleToClipboard()
	{
		try
		{
			this->MainTabManager.CutSingleBook();
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
		catch (const out_of_range_book&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoBook);
		}
	}
	void MenuCopySingleToClipboard()
	{
		try
		{
			this->MainTabManager.CopySingleBook();
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
		catch (const out_of_range_book&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoBook);
		}
	}
	void MenuCutSingleToClipboardFromBin()
	{
		try
		{
			this->MainTabManager.CutSingleBook(0, false, true);
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
		catch (const out_of_range_book&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoBook);
		}
	}
	void MenuCopySingleToClipboardFromBin()
	{
		try
		{
		this->MainTabManager.CopySingleBook(0,false,true);
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
		catch (const out_of_range_book&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoBook);
		}
	}
	void MenuRemoveFromBin()
	{

		try
		{
			this->MainTabManager.GetBin().GetBook(); //Sprawdzanie czy zak�adka istnieje
		}
		catch (const out_of_range_book&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoBook);
			return;
		}

		if (this->MainGUI.YesNoPopup(this->DeleteConfirmMsg))
		{
			this->MainTabManager.GetBin().RemoveOneBook();
		}
	}
	void MenuRestoreBook()
	{
		bool IsParent;
		try
		{
			IsParent = this->MainTabManager.IsExistParentTab(this->MainTabManager.GetBin().GetBook()->GetParentID());
		}
		catch (const out_of_range_book&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoBook);
			return;
		}

		if (IsParent)
		{
			this->MainTabManager.CutSingleBook(0, false, true);
			this->MainGUI.MsgBoxShow(this->RestoringMsg);
		}
		else
		{
			this->MainGUI.MsgBoxShow(this->RestoringErrorMsg);
		}
	}
	void MenuCutManyToClipboard()
	{
		try
		{
			this->MainTabManager.CutManyBooks();
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
	}
	void MenuCopyManyToClipboard()
	{
		try
		{
			this->MainTabManager.CopyManyBooks();
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
	}
	void MenuPasteFromClipboard()
	{
		try
		{
			this->MainTabManager.PasteFromClipboard();
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
		catch (const out_of_range_book&)
		{

		}
	}
	void MenuCheckAll()
	{
		try
		{
			this->MainTabManager.ChangeCheckAllBooks(true);
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}

	}
	void MenuUnCheckAll()
	{
		try
		{
			this->MainTabManager.ChangeCheckAllBooks(false);
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
	}
	void MenuSearchSelectAll()
	{
		try
		{
			this->MainTabManager.ChangeCheckAllFoundBooks(true);
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
	}
	void MenuSearchUnselectAll()
	{
		try
		{
			this->MainTabManager.ChangeCheckAllFoundBooks(false);
		}
		catch (const out_of_range_tab&)
		{
			this->MainGUI.MsgBoxShow(this->ErrorNoOpenFileMsg);
		}
	}
	void ExtraMenuClose()
	{
		this->IsExtraMenu = false;
	}
	void MenuSearchCommand()
	{
		int SelectedItem = this->MainGUI.RadioButton(this->TitleSearch, this->RadioButtonText);
		this->RedrawInterface(kSearchElements);
		std::string ValueChecking;
		if (SelectedItem == 4)
		{
			//TODO: wprowadzanie daty
		}
		else if (SelectedItem == 5 || SelectedItem == 6)
		{
			ValueChecking = this->MainGUI.PopupInputBox(SearchInformationText.at(SelectedItem));
		}
		else if (SelectedItem == 7)
		{
			ValueChecking = std::to_string(this->MainGUI.YesNoPopup(SearchInformationText.at(SelectedItem)));
		}
		else
		{
			ValueChecking = this->MainGUI.PopupInputBox(SearchInformationText.at(SelectedItem));
		}
		this->MainTabManager.GetTab().FoundAll(SelectedItem, ValueChecking);
	}

	void Menu()
	{
		while (this->IsProgramRunning)
		{
			this->RedrawInterface();
			switch (this->MainGUI.ReadKey().VirtualCode)
			{
				case VK_F1:
				{
					this->MenuNewFile();
					break;
				}
				case VK_F2:
				{
					this->MenuOpenFile();
					break;
				}
				case VK_F3:
				{
					this->MenuSaveFile();
					break;
				}
				case VK_F4:
				{
					this->MenuChangeFileName();
					break;
				}
				case VK_F5:
				{
					this->MenuCloseFile();
					break;
				}
				case VK_F6:
				{
					this->MenuBinSubMenu();
					ActualShowingLegend = 0;
					break;
				}
				case VK_F7:
				{
					this->MenuClipboardSubMenu();
					ActualShowingLegend = 0; 
					break;
				}
				case VK_F12:
				{
					this->MenuCloseProgram();
					break;
				}
				case VK_TAB:
				{
					this->MenuChangeLegendBar();
					break;
				}
				case VK_LEFT:
				{
					this->MenuChangeActualTab(-1);
					break;
				}
				case VK_RIGHT:
				{
					this->MenuChangeActualTab(1);
					break;
				}
				case VK_UP:
				{
					try
					{
						this->ChangeActualBook(-1, this->MainTabManager.GetTab());
					}
					catch (const out_of_range_tab&)
					{

					}
					catch (const out_of_range_book&)
					{

					}
					break;
				}
				case VK_DOWN:
				{
					//this->ChangeActualBook(1, *ActualTab);
					try
					{
						this->ChangeActualBook(1, this->MainTabManager.GetTab());
					}
					catch (const out_of_range_book&)
					{

					}
					catch (const out_of_range_tab&)
					{

					}
					break;
				}
				case int('1'):
				{
					this->MenuAddBook();
					break;
				}
				case int('2'):
				{
					this->MenuEditBook();
					break;
				}
				case int('3'):
				{
					this->MenuRemoveActualBook();
					break;
				}
				case int('4'):
				{
					this->MenuSearchSubMenu();
					ActualShowingLegend = 0;
					break;
				}
				case int('5'):
				{
					this->MenuToggleSelectBook();
					break;
				}
				case int('6'):
				{
					this->MenuSortingTab();
					break;
				}
				case int('7'):
				{
					this->MenuCutSingleToClipboard();
					break;
				}
				case int('8'):
				{
					this->MenuCopySingleToClipboard();
					break;
				}
				case int('9'):
				{
					this->MenuPasteFromClipboard();
					break;
				}
				case int('Q') :
				{
					this->MenuCheckAll();
					break;
				}
				case int('W') :
				{
					this->MenuUnCheckAll();
					break;
				}
				case int('E') :
				{
					this->MenuCutManyToClipboard();
					break;
				}
				case int('R') :
				{
					this->MenuCopyManyToClipboard();
					break;
				}
				case int('Y') :
				{
					this->MenuPasteFromClipboard();
					break;
				}
				case int('U') :
				{
					this->MenuRemoveManyBooks();
					break;
				}

			}
		}
	}
	void MenuBinSubMenu()
	{
		this->RedrawInterface(kBinElements);
		this->IsExtraMenu = true;
		while (this->IsProgramRunning && this->IsExtraMenu)
		{
			this->RedrawInterface(kBinElements);
			switch (this->MainGUI.ReadKey().VirtualCode)
			{
			case int('1') :
			{
				this->MainTabManager.GetBin().ClearBooks();
				break;
			}
			case int('2'):
			{
				this->MenuRestoreBook();
				break;
			}
			case int('3'):
			{
				this->MenuCutSingleToClipboardFromBin();
				break;
			}
			case int('4'):
			{
				this->MenuCopySingleToClipboardFromBin();
				break;
			}
			case int('5'):
			{
				this->MenuRemoveFromBin();
				break;
			}
			case int('0'):
			{
				this->ExtraMenuClose();
				break;
			}
			case VK_UP:
			{
				this->ChangeActualBook(-1, this->MainTabManager.GetBin());
				break;
			}
			case VK_DOWN:
			{
				this->ChangeActualBook(1, this->MainTabManager.GetBin());
				break;
			}
			case VK_F12:
			{
				this->MenuCloseProgram();
				break;
			}
			};
		}
	}
	void MenuClipboardSubMenu()
	{
		this->RedrawInterface(kClipboardElements);
		this->IsExtraMenu = true;
		while (this->IsProgramRunning && this->IsExtraMenu)
		{
			this->RedrawInterface(kClipboardElements);
			switch (this->MainGUI.ReadKey().VirtualCode)
			{
				case int('1') :
				{
					this->MainTabManager.GetClipboard().ClearBooks();
					break;
				}
				case int('0') :
				{
					this->ExtraMenuClose();
					break;
				}
				case VK_UP:
				{
					this->ChangeActualBook(-1, this->MainTabManager.GetClipboard());
					break;
				}
				case VK_DOWN:
				{
					this->ChangeActualBook(1, this->MainTabManager.GetClipboard());
					break;
				}
				case VK_F12:
				{
					this->MenuCloseProgram();
					break;
				}
			};
		}
	}
	void MenuSearchSubMenu()
	{
		try
		{
			this->MainTabManager.GetTab().ResetSearch();
		}
		catch (const out_of_range_tab&)
		{
			return;
		}
		this->RedrawInterface(kSearchElements);
		this->IsExtraMenu = true;
		ActualShowingLegend = 0;
		while (this->IsProgramRunning && this->IsExtraMenu)
		{
			this->RedrawInterface(kSearchElements);
			switch (this->MainGUI.ReadKey().VirtualCode)
			{
				case int('1') :
				{
					this->MainTabManager.GetTab().ResetSearch();
					break;
				}
				case int('2') :
				{
					this->MenuSearchCommand();
					break;
				}
				case int('3') :
				{
					this->MenuCutSingleToClipboard();
					break;
				}
				case int('4') :
				{
					this->MenuCopySingleToClipboard();
					break;
				}
				case int('5') :
				{
					this->MenuToggleSelectBook();
					break;
				}
				case int('6') :
				{
					this->MenuRemoveActualBook();
					break;
				}
				case int('0') :
				{
					this->ExtraMenuClose();
					break;
				}
				case int('Q') :
				{
					this->MenuSearchSelectAll();
					break;
				}
				case int('W') :
				{
					this->MenuSearchUnselectAll();
					break;
				}
				case int('E') :
				{
					this->MenuCutManyToClipboard();
					break;
				}
				case int('R') :
				{
					this->MenuCopyManyToClipboard();
					break;
				}
				case VK_UP:
				{
					this->ChangeActualBook(-1, this->MainTabManager.GetTab(), true);
					break;
				}
				case VK_DOWN:
				{
					this->ChangeActualBook(1, this->MainTabManager.GetTab(), true);
					break;
				}
				case VK_TAB:
				{
					if (ActualShowingLegend) ActualShowingLegend = 0;
					else ActualShowingLegend = 1;
				}
			}
		}
	}
public:
	MainMenu()
	{
		MainMenuLegend.push_back(MainMenuTabLegend);
		MainMenuLegend.push_back(MainMenuSingleLegend);
		MainMenuLegend.push_back(MainMenuGroupLegend);

		SearchMenuLegend.push_back(SearchMenuSingleLegend);
		SearchMenuLegend.push_back(SearchMenuGroupLegend);
	}
	void Start()
	{
		this->Menu();
	}
};