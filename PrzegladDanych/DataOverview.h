#pragma once
#include <iostream>
#include <string>

#include "DataOverviewExceptions.h"

namespace DataOverview
{
	class Date
	{
	private:
		int Year = 0;
		int Month = 0;
		int Day = 0;
		void SetYear(int year)
		{
			if (year < 1 || year > 3000)
			{
				throw date_invalid_range("Year must be in range 1000 to 3000");
			}
			this->Year = year;
		}
		void SetMonth(int month)
		{
			if (month < 1 || month > 12)
			{
				throw date_invalid_range("Month must be in range 1 to 12");
			}
			this->Month = month;
		}
		void SetDay(int day)
		{
			if (day < 1 || day > 31)
			{
				throw date_invalid_range("Day must be in range 1 to 31");
			}
			this->Day = day;
		}
	public:
		Date() {};
		Date(int year, int month, int day)
		{
			this->Year = year;
			this->Month = month;
			this->Day = day;
		}
		Date(std::string textDate)
		{
			this->StringToDate(textDate);
		}
		Date(const Date &source) //konstuktor kopiuj�cy
		{
			this->Year = source.Year;
			this->Month = source.Month;
			this->Day = source.Day;
		}
		void StringToDate(std::string textDate)
		{
			if (textDate.length() < 6 || textDate.length() > 11 || std::count(textDate.begin(), textDate.end(), '-') != 2)
			{
				throw date_invalid_format("Date string must be in fomrat YYYY-MM-DD");
			}
			int FirstSeparator = textDate.find('-', 0);
			int SecondSeparator = textDate.find('-', FirstSeparator + 1);
			this->SetYear(std::stoi(textDate.substr(0, FirstSeparator)));
			this->SetMonth(std::stoi(textDate.substr(FirstSeparator + 1, SecondSeparator)));
			this->SetDay(std::stoi(textDate.substr(SecondSeparator + 1, textDate.length())));
		}
		std::string ToString()
		{
			return std::to_string(this->Year) + "-" + std::to_string(this->Month) + "-" + std::to_string(this->Day);
		}
		int Compare(const Date &second)
		{
			if (this->Year - second.Year) return this->Year - second.Year;
			if (this->Month - second.Month) return this->Month - second.Month;
			if (this->Day - second.Day) return this->Day - second.Day;
			return 0;
		}
		~Date() {}
	};
}