#pragma once
#include "DataOverviewExceptions.h"
#include "SimpleBook.h"
#include "DataOverview.h"

#include <iostream>
#include <exception>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>
#include <ctime>

class OneTab
{
private:
	inline static size_t& lastTabID() { static size_t id = 0; return id; } //HACK: Statyczna zmienna w header only programie
protected:
	std::string FileName = "";
	std::vector<ComplexBook> BooksList;
	size_t ActualBookPos = 0;
	size_t TabID;
	bool TabChanged = true;
public:
	OneTab()
	{
		/*BooksList.resize(20);*/
		this->TabID = ++lastTabID();
	};
	OneTab(const OneTab &source)
	{
		this->FileName = source.FileName;
		this->ActualBookPos = source.ActualBookPos;
		this->BooksList = source.BooksList;
		this->TabID = source.TabID;
	}
	std::string GetFileName() { return this->FileName; }
	void SetFileName(std::string fileName)
	{
		try
		{
			if (fileName.length() < 1) throw invalid_filename("File name must have more than 0 characters");
		}
		catch (const invalid_filename&){} //HACK: Bez tego �apanie tego wyj�tku w mainmenu.h nie powiedzie si� 
		TabChanged = true;
		this->FileName = fileName;
	}
	ComplexBook* GetBook(size_t number = 0)
	{
		if (number == 0) number = this->GetActualBookNumber();
		try
		{
			return &(BooksList.at(number - 1));
		}
		catch (const std::out_of_range&)
		{
			throw out_of_range_book("No book at position");
		}
	}
	ComplexBook* GetBookByOffset(int offset = 0)
	{
		size_t ActualBookNumber = this->GetActualBookNumber() + offset;
		try
		{
			return &(BooksList.at(ActualBookNumber - 1));
		}
		catch(const std::out_of_range&)
		{
			throw out_of_range_book("No book with offset");
		}
	}
	size_t GetActualBookNumber()
	{
		this->UpdateActualBookNumber();
		return this->ActualBookPos;
	}
	size_t GetActualBooksAmount()
	{
		return this->BooksList.size();
	}
	void UpdateActualBookNumber()
	{
		if (this->BooksList.size() == 0)
		{
			this->ActualBookPos = 1;
			return;
		}
		else if (this->ActualBookPos < 1)
		{
			this->ActualBookPos = 1;
			return;
		}
		else if (this->ActualBookPos > this->BooksList.size())
		{
			this->ActualBookPos = this->BooksList.size();
			return;
		}
	}
	size_t GetID()
	{
		return this->TabID;
	}
};

class OneTabOperations : public OneTab
{
protected:
	void SetTabChanged(bool value) { TabChanged = value; }
public:
	bool IsTabChanged() { return TabChanged; }
	void ChangeActualBook(int offset)
	{
		if (this->BooksList.size() == 0)
		{
			throw empty_list("Empty book list");
		}
		int PositionAfterOffset = this->ActualBookPos + offset;
		if ((PositionAfterOffset < 1) || (PositionAfterOffset > int(this->BooksList.size())))
		{
			throw out_of_range_book("Out of range books in tab");
		}
		else
		{
			this->ActualBookPos = PositionAfterOffset;
		}
	}
	void ToggleSelectBook(size_t bookPos = 0)
	{
		if (bookPos == 0) bookPos = this->ActualBookPos;
		this->BooksList.at(bookPos - 1).ToggleSelect();
	}
	void SelectBook(size_t bookPos = 0)
	{
		if (bookPos == 0) bookPos = this->ActualBookPos;
		this->BooksList.at(bookPos - 1).SetSelect(true);
	}
	void UnSelectBook(size_t bookPos = 0)
	{
		if (bookPos == 0) bookPos = this->ActualBookPos;
		this->BooksList.at(bookPos - 1).SetSelect(false);
	}
	void SetSelectAllBooks(bool set = true)
	{
		for (size_t i = 0; i < this->BooksList.size(); i++)
		{
			this->BooksList.at(i).SetSelect(set);
		}
	}
	ComplexBook GetCopyOneBook(int bookPos = 0)
	{
		if (bookPos == 0) bookPos = this->ActualBookPos;
		return this->BooksList.at(bookPos - 1);
	}
	void RemoveOneBook(size_t bookPos = 0)
	{
		if (bookPos == 0) bookPos = this->ActualBookPos;
		if (this->BooksList.size() < bookPos || !this->BooksList.size()) throw out_of_range_book("No book to remove");
		this->BooksList.erase(this->BooksList.begin() + bookPos - 1);
		this->UpdateActualBookNumber();
		this->TabChanged = true;
	}
	void AddBook(ComplexBook& oneBook, bool addOnEnd = true)
	{
		if (addOnEnd) this->BooksList.push_back(oneBook);
		else this->BooksList.insert(this->BooksList.begin() + this->GetActualBookNumber(), oneBook);
		this->UpdateActualBookNumber();
		this->TabChanged = true;
	}
	void AddBook(std::string title, std::string autors, std::string genre, std::string publisher, std::string textReleaseDate, int publishYear, int pages, bool hardcovered, std::string releaseLang, bool addOnEnd = true)
	{
		auto tempBook = ComplexBook(title, autors, genre, publisher, textReleaseDate, publishYear, pages, hardcovered, releaseLang);
		this->AddBook(tempBook, true);
	}
	std::string GetPrettyName()
	{
		return (TabChanged ? "*" : "") + this->FileName;
	}
	void ClearBooks()
	{
		this->BooksList.clear();
	}
};

class OneTabSort : public OneTabOperations
{
private:
	void SortingData(bool(*sortFunc)(ComplexBook&, ComplexBook&))
	{
		int Size = this->BooksList.size();
		int i, j;
		ComplexBook Key;
		for (i = 1; i < Size; i++)
		{
			Key = BooksList.at(i);
			j = i - 1;
			while (j >= 0 && ((*sortFunc)(BooksList.at(j), Key)))
			{
				BooksList.at(j + 1) = BooksList.at(j);
				j = j - 1;
			}
			BooksList.at(j + 1) = Key;
		}
		this->SetTabChanged(true);
	}
	static bool CompareByTitle(ComplexBook &firstBook, ComplexBook &secondBook) { return firstBook.GetTitle().compare(secondBook.GetTitle()) > 0; }
	static bool CompareByAutors(ComplexBook &firstBook, ComplexBook &secondBook) { return firstBook.GetAutors().compare(secondBook.GetAutors()) > 0; }
	static bool CompareByGenre(ComplexBook &firstBook, ComplexBook &secondBook) { return firstBook.GetGenre().compare(secondBook.GetGenre()) > 0; }
	static bool CompareByPublisher(ComplexBook &firstBook, ComplexBook &secondBook) { return firstBook.GetPublisher().compare(secondBook.GetPublisher()) > 0; }
	static bool CompareByReleaseDate(ComplexBook &firstBook, ComplexBook &secondBook) { return firstBook.GetReleaseDate().Compare(secondBook.GetReleaseDate()) > 0; };
	static bool CompareByPublishYear(ComplexBook &firstBook, ComplexBook &secondBook) { return firstBook.GetPublishYear() > secondBook.GetPublishYear(); }
	static bool CompareByPages(ComplexBook &firstBook, ComplexBook &secondBook) { return firstBook.GetPages() > secondBook.GetPages(); }
	static bool CompareByHardCovered(ComplexBook &firstBook, ComplexBook &secondBook) { return firstBook.GetHardCoveredBook() > secondBook.GetHardCoveredBook(); }
	static bool CompareByReleaseLang(ComplexBook &firstBook, ComplexBook &secondBook) { return firstBook.GetReleaseLang().compare(secondBook.GetReleaseLang()) > 0; }

public:
	void SortTab(int sortBy)
	{
		if (sortBy == 0) SortingData(&OneTabSort::CompareByTitle);
		else if (sortBy == 1) SortingData(&OneTabSort::CompareByAutors);
		else if (sortBy == 2) SortingData(&OneTabSort::CompareByGenre);
		else if (sortBy == 3) SortingData(&OneTabSort::CompareByPublisher);
		else if (sortBy == 4) SortingData(&OneTabSort::CompareByReleaseDate);
		else if (sortBy == 5) SortingData(&OneTabSort::CompareByPublishYear);
		else if (sortBy == 6) SortingData(&OneTabSort::CompareByPublishYear);
		else if (sortBy == 7) SortingData(&OneTabSort::CompareByHardCovered);
		else if (sortBy == 8) SortingData(&OneTabSort::CompareByReleaseLang);
	}
};

class OneTabFile : public OneTabSort
{
private:
	std::string SaveKey;
	int LastSaveTime = 0;
	int SaveType = 0;
public:
	void SetType(int type)
	{
		switch (type)
		{
		case 0: //CSV format
			this->SaveType = type;
			break;
		default:
			throw invalid_save_extension("Wrong write type");
		}
		this->SetTabChanged(true);
	}
	std::string GetExtAsString()
	{
		if (this->SaveType == 0) return "csv";
		return "";
	}
	void LoadData()
	{
		std::stringstream ContentsStream;
		std::ifstream LocalFile(this->GetFileNameWithExt(), std::ios::binary);
		if (!LocalFile.is_open()) {
			throw (std::runtime_error("Open file error"));
		}
		// Kopiuje zawarto�� pliku do stringstream
		ContentsStream << LocalFile.rdbuf();
		LocalFile.close();
		ContentsStream.clear();

		std::string OneLine;
		std::stringstream OneLineStream;
		while (std::getline(ContentsStream, OneLine))
		{
			this->CsvLineToBooksList(OneLine);
		}
		this->LastSaveTime = int(std::time(nullptr));
		this->UpdateActualBookNumber();
		this->SetTabChanged(false);
	}
	void LoadData(std::string fileName, int saveType, std::string key)
	{
		this->FileName = fileName;
		this->SaveType = saveType;
		this->SaveKey = key;
		this->LoadData();
	}
	void SaveData()
	{
		std::ofstream LocalFile(this->GetFileNameWithExt(), std::ios::binary);
		if (!LocalFile.is_open()) {
			throw (std::runtime_error("Open file Error"));
		}
		if (this->SaveType == 0)
		{
			this->WriteToCSV(LocalFile);
		}
		LocalFile.close();
		this->LastSaveTime = int(std::time(nullptr));
		this->SetTabChanged(false);
	}
	void SaveData(std::string fileName, int saveType, std::string key)
	{
		this->FileName = fileName;
		this->SaveType = saveType;
		this->SaveKey = key;
		this->SaveData();
	}
	std::string GetFileNameWithExt()
	{
		if (this->FileName.length() < 1) throw invalid_filename("File name must have more than 0 characters");
		std::string Ext = this->GetExtAsString();
		return this->FileName + "." + Ext;
	}
	void WriteToCSV(std::ofstream& localFile)
	{
		std::string OneLine;
		for (ComplexBook OneBook : this->BooksList)
		{
			OneLine.clear();
			OneLine.append(OneBook.GetTitle());
			OneLine.append(";");
			OneLine.append(OneBook.GetAutors());
			OneLine.append(";");
			OneLine.append(OneBook.GetGenre());
			OneLine.append(";");
			OneLine.append(OneBook.GetPublisher());
			OneLine.append(";");
			OneLine.append(OneBook.GetReleaseDate().ToString());
			OneLine.append(";");
			OneLine.append(std::to_string(OneBook.GetPublishYear()));
			OneLine.append(";");
			OneLine.append(std::to_string(OneBook.GetPages()));
			OneLine.append(";");
			if (OneBook.GetHardCoveredBook()) OneLine.append("twarda");
			else OneLine.append("miekka");
			OneLine.append(";");
			OneLine.append(OneBook.GetReleaseLang());
			//std::cout << OneLine << std::endl;
			localFile << OneLine << "\r\n";
		}
	}
	void CsvLineToBooksList(std::string& oneLine)
	{
		if (oneLine[0] != '/' && oneLine[1] != '/')
		{
			int FirstSeparator = 0;
			int SecondSeparator = 0;
			std::string OneBook[9];
			for (int i = 0; i < 8; i++)
			{
				SecondSeparator = oneLine.find(';', FirstSeparator);
				std::string OneElement = oneLine.substr(FirstSeparator, SecondSeparator - FirstSeparator);
				OneElement = this->RemoveSurroundingSpaces(OneElement);
				OneBook[i] = OneElement;
				FirstSeparator = SecondSeparator + 1;
			}
			OneBook[8] = this->RemoveSurroundingSpaces(oneLine.substr(FirstSeparator, oneLine.length() - 1 - FirstSeparator));
			//std::cout << "ADDING BOOK" << std::endl;
			this->AddBookFromStringArray(OneBook);
		}
	}
	void AddBookFromStringArray(std::string oneBook[], bool addOnEnd = true)
	{
		bool IsHardCovered;
		if (oneBook[7].compare("miekka") == 0) IsHardCovered = false;
		else if (oneBook[7].compare("twarda") == 0) IsHardCovered = true;
		else throw no_valid_data("Hardcovered should be word 'miekka' or 'twarda'");
		try
		{
			this->AddBook(oneBook[0], oneBook[1], oneBook[2], oneBook[3], oneBook[4], std::stoi(oneBook[5]), std::stoi(oneBook[6]), IsHardCovered, oneBook[8], addOnEnd);
		}
		catch(const std::invalid_argument& e)
		{
			throw no_valid_data(e.what());
		}
		//									//Tytul; Autorzy; Gatunek; Wydawnictwo; Data premiery; Rok wydania; Ilosc stron; Oprawa; Jezyk wydania
	}
	std::string RemoveSurroundingSpaces(std::string text)
	{
		while (isspace(text[0])) text.erase(text.begin(), text.begin() + 1);
		while (isspace(text[text.length() - 1])) text.erase(text.end() - 1, text.end());
		return text;
	}
};

class OneTabSearch : public OneTabFile
{
private:
	static std::string GetTitle(ComplexBook &firstBook) { return firstBook.GetTitle(); }
	static std::string GetAutors(ComplexBook &firstBook) { return firstBook.GetAutors(); }
	static std::string GetGenre(ComplexBook &firstBook) { return firstBook.GetGenre(); }
	static std::string GetPublisher(ComplexBook &firstBook) { return firstBook.GetPublisher(); }
	static DataOverview::Date GetReleaseDate(ComplexBook &firstBook) { return firstBook.GetReleaseDate(); }
	static int GetPublishYear(ComplexBook &firstBook) { return firstBook.GetPublishYear(); }
	static int GetPages(ComplexBook &firstBook) { return firstBook.GetPages(); }
	static bool GetHardCovered(ComplexBook &firstBook) { return firstBook.GetHardCoveredBook(); }
	static std::string GetReleaseLang(ComplexBook &firstBook) { return firstBook.GetReleaseLang(); }

	static bool StringCompare(std::string first, std::string second, int typeCompare)
	{
		return (first.find(second, 0) != std::string::npos);
	}
	static bool IntCompare(int first, int second, int typeCompare)
	{
		if (typeCompare == 0) return first == second;
		else if (typeCompare == 1) return first >= second;
		else if (typeCompare == 2) return first > second;
		else if (typeCompare == 3) return first <= second;
		else if (typeCompare == 4) return first < second;
		throw invalid_tab_argument("Wrong TypeCompare type");
	}
	static bool BoolCompare(bool first, bool second, int typeCompare)
	{
		return first == second;
	}
	static bool DateCompare(DataOverview::Date first, DataOverview::Date second, int typeCompare)
	{
		if (typeCompare == 0) return first.Compare(second) == 0;
		else if (typeCompare == 1) return first.Compare(second) >= 0;
		else if (typeCompare == 2) return first.Compare(second) > 0;
		else if (typeCompare == 3) return first.Compare(second) <= 0;
		else if (typeCompare == 4) return first.Compare(second) < 0;
	}
	size_t GetSearchIndexNumber(size_t actualNumber)
	{
		size_t ReturnValue= 0;
		for (size_t i = 0; i < actualNumber; i++)
		{
			if (this->BooksList.at(i).IsFound())
			{
				ReturnValue++;
			}
		}
		return ReturnValue;
	}
	void SetFoundAllBooks(bool set)
	{
		for (size_t i = 0; i < this->BooksList.size(); i++)
		{
			this->BooksList.at(i).SetFound(set);
		}
	}

	int GetTypeCompareForNumber(char firstLetter, char secondLetter)
	{
		if (firstLetter == char('=')) return 0;
		else if (firstLetter == char('>'))
		{
			if (secondLetter == char('='))
			{
				return 1;
			}
			return 2;
		}
		else if (firstLetter == char('<'))
		{
			if (secondLetter == char('='))
			{
				return 3;
			}
			return 4;
		}
		throw invalid_tab_argument("Wrong first char");
	}
	
	template<class dataType>
	void CheckAndFound(bool(*checkFunc)(dataType, dataType, int), dataType(*dataReturn)(ComplexBook&), dataType valueChecked, int typeCompare)
	{
		for (size_t i = 0; i < this->BooksList.size(); i++)
		{
			if (!(*checkFunc)((*dataReturn)(this->BooksList.at(i)), valueChecked, typeCompare))
			{
				this->BooksList.at(i).SetFound(false);
			}
		}
	}

	size_t GetSearchBooksAmount()
	{
		size_t amount = 0;
		for (size_t i = 0; i < this->BooksList.size(); i++)
		{
			if (this->BooksList.at(i).IsFound()) amount++;
		}
		return amount;
	}

public:
	void PrepareForSearch()
	{
		this->SetSelectAllBooks(false);
	}
	void ResetSearch()
	{
		this->SetSelectAllBooks(false);
		this->SetFoundAllBooks(true);
	}

	ComplexBook* GetFoundBookByOffset(int offset = 0)
	{
		size_t ActualBookNumber = this->GetActualBookNumber();
		int ActualOffset = offset;
		if (ActualOffset >= 0)
		{
			for (size_t i = ActualBookNumber; i < this->GetActualBooksAmount() + 1; i++)
			{
				if (this->GetBook(i)->IsFound())
				{
					if (ActualOffset <= 0)
					{
						return &(BooksList.at(i - 1));
					}
					ActualOffset--;
				}
			}
		}
		else
		{
			for (size_t i = ActualBookNumber; i >= 0; i--)
			{
				if (this->GetBook(i)->IsFound())
				{
					if (ActualOffset >= 0)
					{
						return &(BooksList.at(i - 1));
					}
					ActualOffset++;
				}
			}
		}
		throw out_of_range_book("No element at this index");
	}

	void ChangeActualBookSearch(int offset)
	{
		if (this->GetSearchBooksAmount() == 0)
		{
			throw empty_list("Empty book list");
		}

		size_t ActualBookNumber = this->GetActualBookNumber();
		int ActualOffset = offset;
		if (ActualOffset >= 0)
		{
			for (size_t i = ActualBookNumber; i < this->GetActualBooksAmount() + 1; i++)
			{
				if (this->GetBook(i)->IsFound())
				{
					if (ActualOffset <= 0)
					{
						this->ActualBookPos = i;
						return;
					}
					ActualOffset--;
				}
			}
		}
		else
		{
			for (size_t i = ActualBookNumber; i >= 0; i--)
			{
				if (this->GetBook(i)->IsFound())
				{
					if (ActualOffset >= 0)
					{
						this->ActualBookPos = i;
						return;
					}
					ActualOffset++;
				}
			}
		}
		throw out_of_range_book("No books at this index");
	}
	void SetSelectAllFoundBooks(bool set = true)
	{
		for (size_t i = 0; i < this->BooksList.size(); i++)
		{
			if (this->BooksList.at(i).IsFound())
			{
				this->BooksList.at(i).SetSelect(set);
			}
		}
	}
	void FoundAll(int type, std::string valueToCompare)
	{
		if (type == 0)
		{
			this->CheckAndFound(&OneTabSearch::StringCompare, &OneTabSearch::GetTitle, valueToCompare, 0);
		}
		else if (type == 1)
		{
			this->CheckAndFound(&OneTabSearch::StringCompare, &OneTabSearch::GetAutors, valueToCompare, 0);
		}
		else if (type == 2)
		{
			this->CheckAndFound(&OneTabSearch::StringCompare, &OneTabSearch::GetGenre, valueToCompare, 0);
		}
		else if (type == 3)
		{
			this->CheckAndFound(&OneTabSearch::StringCompare, &OneTabSearch::GetPublisher, valueToCompare, 0);
		}
		else if (type == 4)
		{
			//this->CheckAndFound(&OneTabSearch::DateCompare, &OneTabSearch::GetReleaseDate, valueToCompare, 0);
		}
		else if (type == 5)
		{
			int compareType = GetTypeCompareForNumber(valueToCompare[0], valueToCompare[1]);
			std::string CroppedValue = compareType % 2 ? valueToCompare.substr(2, valueToCompare.length() - 2) : valueToCompare.substr(1, valueToCompare.length() - 1);
			this->CheckAndFound(&OneTabSearch::IntCompare, &OneTabSearch::GetPublishYear, std::stoi(CroppedValue), compareType);
		}
		else if (type == 6)
		{
			int compareType = this->GetTypeCompareForNumber(valueToCompare[0], valueToCompare[1]);
			std::string CroppedValue = compareType % 2 ? valueToCompare.substr(2, valueToCompare.length() - 2) : valueToCompare.substr(1, valueToCompare.length() - 1);
			this->CheckAndFound(&OneTabSearch::IntCompare, &OneTabSearch::GetPages, std::stoi(CroppedValue), compareType);
		}
		else if (type == 7)
		{
			bool ValueTemp = valueToCompare == "1" ? true: false;
			this->CheckAndFound(&OneTabSearch::BoolCompare, &OneTabSearch::GetHardCovered, ValueTemp, 0);
		}
		else if (type == 8)
		{
			this->CheckAndFound(&OneTabSearch::StringCompare, &OneTabSearch::GetReleaseLang, valueToCompare, 0);
		}
	}

	//void CheckAllRemaindsElements

};