#pragma once
#include "Tab.h"

#include <vector>
#include <string>
#include "DataOverviewExceptions.h"


class TabManager
{
private:
	OneTabSearch Bin;
	OneTabSearch Clipboard;
	std::vector<OneTabSearch> TabList;
	std::vector<std::string> TabsNames;
	size_t ActualTabNumber = 0;

	void UpdateNamesList()
	{
		TabsNames.clear();
		for (OneTabSearch& Tab : TabList)
		{
			TabsNames.push_back(Tab.GetPrettyName());
		}
	}
public:
	void UpdateActualTabNumer()
	{
		if (this->TabList.size() == 0) this->ActualTabNumber = 0;
		else if (this->TabList.size() < this->ActualTabNumber) this->ActualTabNumber = this->TabList.size();
	}
	size_t GetActualTabNumber()
	{
		this->UpdateActualTabNumer();
		return this->ActualTabNumber;
	}
	size_t GetActualTabsAmount()
	{
		return this->TabList.size();
	}
	void SetActualTabNumber(size_t tabNumber)
	{
		if (tabNumber > this->TabList.size() && tabNumber < 1) throw out_of_range_tab("Out of range TabList");
		this->ActualTabNumber = tabNumber;
	}
	void ChangeActualTabNumer(int offset)
	{
		if (this->ActualTabNumber == 0)
		{
			throw empty_list("Empty tab list");
		}
		int PositionAfterOffset = this->ActualTabNumber + offset;
		if ((PositionAfterOffset < 1) || (PositionAfterOffset > int(this->TabList.size())))
		{
			throw out_of_range_tab("Out of range books in tab");
		}
		else
		{
			this->ActualTabNumber = PositionAfterOffset;
		}
	}
	void AddNewTab()
	{
		TabList.push_back(OneTabSearch());
		SetActualTabNumber(TabList.size());
	}
	void RemoveTab(size_t tabNumber = 0)
	{
		if (tabNumber == 0) tabNumber = this->ActualTabNumber;
		TabList.erase(TabList.begin() + tabNumber - 1);
	}
	OneTabSearch& GetTab(size_t tabNumber = 0)
	{
		if (tabNumber == 0) tabNumber = this->ActualTabNumber;
		try
		{
			return TabList.at(tabNumber - 1);
		}
		catch (const std::out_of_range&)
		{
			throw out_of_range_tab("No item at index");
		}
	}
	OneTabSearch& GetBin()
	{
		return this->Bin;
	}
	OneTabSearch& GetClipboard()
	{
		return this->Clipboard;
	}
	std::vector<std::string>* GetTabNamesVector()
	{
		UpdateNamesList();
		return &TabsNames;
	}

	void CopySingleBook(size_t bookNumber = 0, bool toBin = false, bool fromBin = false)
	{
		ComplexBook CopyBook;
		if (fromBin) CopyBook = *this->GetBin().GetBook();
		else CopyBook = *this->GetTab().GetBook(bookNumber);

		if (toBin)
		{
			CopyBook.SetParentID(this->GetTab().GetID());
			this->Bin.AddBook(CopyBook);
		}
		else
		{
			this->Clipboard.ClearBooks();
			this->Clipboard.AddBook(CopyBook);
		}
	}
	void CutSingleBook(size_t bookNumber = 0, bool toBin = false, bool fromBin = false) //TODO: Do poprawy
	{
		this->CopySingleBook(bookNumber, toBin, fromBin);
		if (fromBin) this->GetBin().RemoveOneBook(bookNumber);
		else this->GetTab().RemoveOneBook(bookNumber);
	}
	void CopyManyBooks(bool toBin = false)
	{
		this->Clipboard.ClearBooks();
		ComplexBook CopyBook;
		for (size_t i = 1; i <= this->GetTab().GetActualBooksAmount(); i++)
		{
			if (this->GetTab().GetBook(i)->IsSelected())
			{
				CopyBook = *this->GetTab().GetBook(i);
				if (toBin)
				{
					CopyBook.SetParentID(this->GetTab().GetID());
					this->Bin.AddBook(CopyBook);
				}
				else this->Clipboard.AddBook(CopyBook);
			}
		}
	}
	void CutManyBooks(bool toBin = false)
	{
		this->CopyManyBooks(toBin);
		for (size_t i = this->GetTab().GetActualBooksAmount(); i > 0; i--)
		{
			if (this->GetTab().GetBook(i)->IsSelected())
			{
				this->GetTab().RemoveOneBook(i);
			}
		}
	}
	void PasteFromClipboard()
	{
		for (size_t i = 0; i <= this->Clipboard.GetActualBooksAmount(); i++)
		{
			this->GetTab().AddBook(*this->Clipboard.GetBook(i));
			//this->Clipboard.RemoveOneBook(1);
		}
	}
	void ChangeCheckAllBooks(bool check = true)
	{
		this->GetTab().SetSelectAllBooks(check);
	}
	void ChangeCheckAllFoundBooks(bool check = true)
	{
		this->GetTab().SetSelectAllFoundBooks(check);
	}
	bool IsExistParentTab(size_t parentID)
	{
		for (size_t i = 1; i <= this->GetActualTabsAmount(); i++)
		{
			size_t id = this->GetTab(i).GetID();
			if (id == parentID)
			{
				return true;
			}
		}
		return false;
 	}
};