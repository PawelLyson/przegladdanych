#pragma once
#include "DataOverview.h"

#include <string>
#include <vector>
#include <exception>

class SimpleBook
{
protected:
	std::string Title = "";
	std::string Autors = "";
	std::string Genre = "";
	std::string Publisher = "";
	DataOverview::Date ReleaseDate;
	int PublishYear = 0;
	int Pages = 0;
	bool HardCoveredBook = false;
	std::string ReleaseLang = "";

	void SetBook(std::string title, std::string autors, std::string genre, std::string publisher, std::string textReleaseDate, int publishYear, int pages, bool hardCovered, std::string releaseLang)
	{
		this->Title = title;
		this->Autors = autors;
		this->Genre = genre;
		this->Publisher = publisher;
		this->SetReleaseDate(textReleaseDate);
		this->PublishYear = publishYear;
		this->Pages = pages;
		this->HardCoveredBook = hardCovered;
		this->ReleaseLang = releaseLang;
	}
	void SetBook(std::string title, std::string autors, std::string genre, std::string publisher, DataOverview::Date releaseDate, int publishYear, int pages, bool hardCovered, std::string releaseLang)
	{
		this->SetBook(title, autors, genre, publisher, releaseDate.ToString(), publishYear, pages, hardCovered, releaseLang);
		this->ValidateObject();
	}

	void ValidateObject()
	{
		if (this->Title.length() == 0) throw no_valid_data("Name should be longer than 0 chars.");
		if (this->Autors.length() == 0) throw no_valid_data("Autors should be longer than 0 chars.");
		if (this->Genre.length() == 0) throw no_valid_data("Genre should be longer than 0 chars.");
		if (this->Publisher.length() == 0) throw no_valid_data("Publisher should be longer than 0 chars.");
		if (this->ReleaseLang.length() == 0) throw no_valid_data("ReleaseLang should be longer than 0 chars.");
		if (this->PublishYear <= 0) throw no_valid_data("PublishYear should be bigger than 0 chars.");
		if (this->Pages <= 0) throw no_valid_data("Pages should be bigger than 0 chars.");
	}

public:
	void SetTitle(std::string title) { this->Title = title; }
	void SetAutors(std::string autors) { this->Autors = autors; }
	void SetGenre(std::string genre) { this->Genre = genre; }
	void SetPublisher(std::string publisher) { this->Publisher = publisher; }
	void SetReleaseDate(DataOverview::Date releaseDate) { this->ReleaseDate = releaseDate; }
	void SetPublishYear(int publishYear) { this->PublishYear = publishYear; }
	void SetPages(int pages) { this->Pages = pages; }
	void SetHardCoveredBook(bool hardCoveredBook) { this->HardCoveredBook = hardCoveredBook; }
	void SetReleaseLang(std::string releaseLang) { this->ReleaseLang = releaseLang; }

	std::string GetTitle() { return this->Title; }
	std::string GetAutors() { return this->Autors; }
	std::string GetGenre() { return this->Genre; }
	std::string GetPublisher() { return this->Publisher; }
	DataOverview::Date GetReleaseDate() { return this->ReleaseDate; }
	int GetPublishYear() { return this->PublishYear; }
	int GetPages() { return this->Pages; }
	bool GetHardCoveredBook() { return this->HardCoveredBook; }
	std::string GetReleaseLang() { return this->ReleaseLang; }

	std::vector<std::string> GetBookAsStringVector()
	{
		std::vector<std::string> Book = { this->Title, this->Autors, this->Genre, this->Publisher, this->ReleaseDate.ToString(), std::to_string(this->PublishYear), std::to_string(this->Pages), (this->HardCoveredBook ? "twarda" : "miekka"),  this->ReleaseLang };
		return Book;
	}
};

class ComplexBook : public SimpleBook
{
private:
	size_t ParentID = 0;
	bool _select = false;
	bool _isFound = true; 
public:
	ComplexBook() {};
	ComplexBook(std::string title, std::string autors, std::string genre, std::string publisher, std::string textReleaseDate, int publishYear, int pages, bool hardcovered, std::string releaseLang)
	{
		this->SetBook(title, autors, genre, publisher, textReleaseDate, publishYear, pages, hardcovered, releaseLang);
	}
	ComplexBook(const ComplexBook &source)
	{
		this->SetBook(source.Title, source.Autors, source.Genre, source.Publisher, source.ReleaseDate, source.PublishYear, source.Pages, source.HardCoveredBook, source.ReleaseLang);
	}
	//~ComplexBook() {	/*std::cout << "Delete book " << this->Title << std::endl;*/ };

	void ToggleSelect() { this->_select = !this->_select; }
	void SetSelect(bool set = true) { this->_select = set; }
	void SetFound(bool set = true) { this->_isFound = set; }

	bool IsSelected() { return this->_select; }
	bool IsFound() { return this->_isFound; }

	size_t GetParentID() { return this->ParentID; }
	void SetParentID(size_t id) { this->ParentID = id; }
};

