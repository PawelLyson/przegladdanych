#pragma once
#include <exception>
class DataOverviewExceptions : std::exception
{
private:
	char const* ShowMessage;
public:
	class DataOverviewExceptions(char const* const msg)
	{
		ShowMessage = msg;
	}
	virtual const char* what() const throw()
	{
		return ShowMessage;
	}
};

class invalid_filename : DataOverviewExceptions
{
public:
	invalid_filename(char const* const msg) : DataOverviewExceptions(msg) {};
};

class invalid_save_extension : DataOverviewExceptions
{
public:
	invalid_save_extension(char const* const msg) : DataOverviewExceptions(msg) {};
};

class invalid_tab_argument : DataOverviewExceptions
{
public:
	invalid_tab_argument(char const* const msg) : DataOverviewExceptions(msg) {};
};

class empty_list : DataOverviewExceptions
{
public:
	empty_list(char const* const msg) : DataOverviewExceptions(msg) {};
};

class out_of_range_tab : DataOverviewExceptions
{
public:
	out_of_range_tab(char const* const msg) : DataOverviewExceptions(msg) {};
};

class out_of_range_book : DataOverviewExceptions
{
public:
	out_of_range_book(char const* const msg) : DataOverviewExceptions(msg) {};
};

class invalid_input_type_argument : DataOverviewExceptions
{
public:
	invalid_input_type_argument(char const* const msg) : DataOverviewExceptions(msg) {};
};

class date_invalid_format : DataOverviewExceptions
{
public:
	date_invalid_format(char const* const msg) : DataOverviewExceptions(msg) {};
};

class date_invalid_range : DataOverviewExceptions
{
public:
	date_invalid_range(char const* const msg) : DataOverviewExceptions(msg) {};
};

class no_valid_data : DataOverviewExceptions
{
public:
	no_valid_data(char const* const msg) : DataOverviewExceptions(msg) {};
};